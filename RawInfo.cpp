#include "RawInfo.h"
#include "OutFilestream.h"

RawInfo::RawInfo(void)
	:Timestamped(0),
	configuredWidth(0),
	configuredHeight(0),
	apiVersion(0),
	imageData(0),
	height(0),
	width(0),
	pitch(0),
	frameSize(0),
	bitsPerPixel(0),
	blackLevel(0),
	whiteLevel(0),
	cfaPattern(0),
	calibrationIlluminant(0),
	dynamicRange(0)
{
	jpeg.x = 0;
	jpeg.y = 0;
	jpeg.width = 0;
	jpeg.height = 0;
	activeArea.y1 = 0;
	activeArea.x1 = 0;
	activeArea.y2 = 0;
	activeArea.x2 = 0;
	exposureBias[0] = 0;
	exposureBias[1] = 0;
}

RawInfo::RawInfo(InFilestream& inFile)
	:Timestamped(inFile.readUInt64()),
	configuredWidth(inFile.readUInt16()),
	configuredHeight(inFile.readUInt16())
{
	apiVersion = inFile.readUInt32();
	imageData = inFile.readUInt32();
	height = inFile.readUInt32();
	width = inFile.readUInt32();
	pitch = inFile.readUInt32();
	frameSize = inFile.readUInt32();
	bitsPerPixel = inFile.readUInt32();
	blackLevel = inFile.readUInt32();
	whiteLevel = inFile.readUInt32();
	jpeg.x = inFile.readUInt32();
	jpeg.y = inFile.readUInt32();
	jpeg.width = inFile.readUInt32();
	jpeg.height = inFile.readUInt32();
	activeArea.y1 = inFile.readUInt32();
	activeArea.x1 = inFile.readUInt32();
	activeArea.y2 = inFile.readUInt32();
	activeArea.x2 = inFile.readUInt32();
	exposureBias[0] = inFile.readUInt32();
	exposureBias[1] = inFile.readUInt32();
	cfaPattern = inFile.readUInt32();
	calibrationIlluminant = inFile.readUInt32();
	inFile.readBytes(&colorMatrix,sizeof(colorMatrix));
	dynamicRange = inFile.readUInt32();
	
	// set active area
	activeArea.y1 = 0;
	activeArea.x1 = 0;
	activeArea.y2 = configuredHeight;
	activeArea.x2 = configuredWidth;

	jpeg.height = configuredHeight;
	jpeg.width = configuredWidth;
}

void RawInfo::write(OutFilestream& outfile)
{
	outfile.writeValue(apiVersion,4);
	outfile.writeValue(imageData,4);
	outfile.writeValue(height,4);
	outfile.writeValue(width,4);
	outfile.writeValue(pitch,4);
	outfile.writeValue(frameSize,4);
	outfile.writeValue(bitsPerPixel,4);
	outfile.writeValue(blackLevel,4);
	outfile.writeValue(whiteLevel,4);
	outfile.writeValue(jpeg.x,4);
	outfile.writeValue(jpeg.y,4);
	outfile.writeValue(jpeg.width ,4);
	outfile.writeValue(jpeg.height,4);
	outfile.writeValue(activeArea.y1,4);
	outfile.writeValue(activeArea.x1,4);
	outfile.writeValue(activeArea.y2,4);
	outfile.writeValue(activeArea.x2,4);
	outfile.writeValue(exposureBias[0],4);
	outfile.writeValue(exposureBias[1],4);
	outfile.writeValue(cfaPattern ,4);
	outfile.writeValue(calibrationIlluminant,4);
	outfile.write(&colorMatrix,sizeof(colorMatrix));
	outfile.writeValue(dynamicRange ,4);
}
