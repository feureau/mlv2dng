#ifndef MLV2DNG_H
#define MLV2DNG_H

#include <cmath>
#include "MagicLanternVideo.h"
#include "DigitalNegative.h"

class Mlv2dng
{
private:
	DigitalNegative dng;

	const VideoFrame& videoFrame;
	const FileHeader& fileHeader;
	const RawInfo& rawInfo;
	const WavInfo& wavInfo;
	const ExposureInfo& exposureInfo;
	const LensInfo& lensInfo;
	const RealTimeClock& realTimeClock;
	const Info& info;
	const Identity& identity;
	const WhiteBalanceInfo& whiteBalanceInfo;

	static const uint32_t dngWidth = 128;
	static const uint32_t dngHeight = 96;

	void Init();
public:
	Mlv2dng(std::string filename, const VideoFrame& videoFrame, const MagicLanternVideo& mlv);

private:
	void createPreviewImageIfd();
	void createMainImageIfd();
	void createExifIfd();
	void setOffsets();

	void writeThumbnail(OutFilestream& outFile);

private: // utility functions
	inline uint32_t powerCalc( int mult, int x, int x_div, double y, int y_div) { return  static_cast<int>(mult * pow( static_cast<double>(x)/x_div, y/y_div )); }
	template<typename T> inline T BigEndian(T value) { return ((value & 0x000000FF) << 24) | ((value & 0x0000FF00) << 8) | ((value & 0x00FF0000) >> 8) | ((value & 0xFF000000) >> 24); }
};

#endif
