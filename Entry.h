#ifndef ENTRY_H
#define ENTRY_H

#include <vector>
#include <string>
#include "TiffTags.h"
#include "EntryType.h"
#include "InFilestream.h"
#include "OutFilestream.h"

typedef std::vector<uint8_t> EntryData;
typedef std::vector<uint8_t> ByteArray;
typedef std::vector<uint16_t> ShortArray;
typedef std::vector<uint32_t> LongArray;

class Entry
{
private:
	Tag tag;
	Type type;
	uint32_t count;
	uint32_t offset;
	EntryData data;

public:
	Entry(Tag tag, Type type, uint32_t count, uint32_t offset, const EntryData& data = EntryData());
	Entry(Tag tag, Type type, uint32_t data);
	Entry(Tag tag, uint8_t data);
	Entry(Tag tag, uint16_t data);
	Entry(Tag tag, uint32_t data);
	Entry(Tag tag, std::string data);
	Entry(Tag tag, ByteArray data);
	Entry(Tag tag, ShortArray data);
	Entry(Tag tag, LongArray data);
	Entry(Tag tag, Type type, LongArray data);

	Entry(InFilestream& file);

	bool isPointer() const;
	void Skip(bool shouldSkip);
	bool Skip() const;
	Type BaseType() const;
	int getTypeSize() const;

	Tag getTag() const;
	Type getType() const;
	uint32_t getCount() const;
	uint32_t getOffset() const;
	const EntryData& getData() const;
	void setTag(Tag tag);
	void setType(Type type);
	void setCount(uint32_t count);
	void setOffset(uint32_t offset);
	void setData(const EntryData& data);
	void setData(const std::vector<uint32_t>& data);
	void setData(const std::vector<uint16_t>& data);
	uint32_t dataSize() const;
	void write(OutFilestream& outFile, uint64_t& dataOffset) const;
};

#endif
