#ifndef BUFFER_H
#define BUFFER_H

#include <cstdint>

template <typename T>
class Buffer
{
private:
	T* buffer;
	Buffer<T>* prev;
	Buffer<T>* next;
public:
	Buffer(uint32_t size)
		:buffer(new T[size]),
		prev(NULL),
		next(NULL)
	{}

	Buffer(Buffer* rhs)
		:buffer(rhs->buffer)
	{
		this->prev = rhs->prev;
		rhs->prev = this;
		this->next = rhs;
	}

	Buffer& operator=(Buffer& rhs)
	{
		if(this != &rhs)
		{
			this->buffer = rhs.buffer;
			this->prev = rhs.prev;
			rhs.prev = this;
			this->next = &rhs;
		}
		return *this;
	}

	Buffer* setPrev(Buffer* other)
	{
		Buffer<T>* currentPrev = this->prev;
		this->prev = other;
		return currentPrev;
	}

	T& operator [](uint32_t index)
	{
		return buffer[index];
	}

	T*& operator &()
	{
		return buffer;
	}

	T*& getPointer()
	{
		return buffer;
	}

	T& operator *() const
	{
		return *buffer;
	}

	T* operator ->() const
	{
		return buffer;
	}

	~Buffer(void)
	{
		if(this->prev == NULL && this->next == NULL)
		{
			delete [] this->buffer;
		}
		if(this->prev != NULL)
		{
			this->prev->next = this->next;
		}
		if(this->next != NULL)
		{
			this->next->prev = this->prev;
		}
	}
};

#endif