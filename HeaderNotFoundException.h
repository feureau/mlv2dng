#ifndef HEADERNOTFOUNDEXCEPTION_H
#define HEADERNOTFOUNDEXCEPTION_H

#include "Exception.h"

class HeaderNotFoundException :	public std::exception
{
private:
	std::string message;
public:
	HeaderNotFoundException(BlockType headerFourCC) noexcept
	{
		char headerAsCString[5];
		std::copy(reinterpret_cast<const char*>(&headerFourCC), reinterpret_cast<const char*>(&headerFourCC) + sizeof(headerFourCC), headerAsCString);
		headerAsCString[4] = 0;
		std::string fourCcCode(headerAsCString);
		message = "Exception! Header not recognized: '" + fourCcCode + "'\n";
	}
	virtual ~HeaderNotFoundException() noexcept {}
};

#endif
