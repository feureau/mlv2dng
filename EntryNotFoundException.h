#ifndef ENTRYNOTFOUNDEXCEPTION_H
#define ENTRYNOTFOUNDEXCEPTION_H

#include "Exception.h"
#include "TiffTags.h"

class EntryNotFoundException :	public Exception
{
public:
	EntryNotFoundException(Tag tag) noexcept
	{
		std::stringstream stream;
		stream <<  "Exception! Ifd tag not found: '"
         << std::setfill ('0') << std::setw(sizeof(Tag)*2) 
         << std::hex << static_cast<uint16_t>(tag)
		 << "'\n";
		message = stream.str();
	}
    virtual ~EntryNotFoundException(void) noexcept {}
};

#endif
